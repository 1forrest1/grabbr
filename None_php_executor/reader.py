#!/usr/bin/env python
import date


try:
    from PIL import Image
except ImportError:
    import Image
import pytesseract

class OCR:
    def __init__(self,imgIn):
        img2 = pytesseract.image_to_string(Image.open(imgIn))

        print(img2)

        d = date.Date(img2)
        eventDate = d.getdate()
        eventStartTime = d.gettime()[0]
        eventEndTime = d.gettime()[1]
        if ("\n" in img2):
            eventTitle = img2.splitlines()[0]
        else:
            eventTitle = "Event"

        if (img2.find('Career Services') != -1): 
            eventLocation = "Career Services"
        elif (img2.find('Park Auditorium') != -1): 
            eventLocation = "Park Auditorium"
        elif (img2.find('Campus Center') != -1): 
            eventLocation = "Campus Center"
        elif (img2.find('Muller Hall') != -1): 
            eventLocation = "Muller Hall"
        elif (img2.find('Muller Chapel') != -1): 
            eventLocation = "Muller Chapel"
        elif (img2.find('Emerson Suites') != -1): 
            eventLocation = "Emerson Suites"
        elif (img2.find('Emerson') != -1): 
            eventLocation = "Emerson Hall"
        elif (img2.find('Klingenstein') != -1): 
            eventLocation = "Klingenstein Lounge"
        elif (img2.find('Taughannock') != -1): 
            eventLocation = "Taughannock Falls Room"
        elif (img2.find('IC Square') != -1): 
            eventLocation = "IC Square"
        elif (img2.find('Kostrinsky') != -1): 
            eventLocation = "Kostrinsky Field"
        elif (img2.find('Williams') != -1): 
            eventLocation = "Williams Hall"
        elif (img2.find('Friends') != -1): 
            eventLocation = "Friends Hall"
        elif (img2.find('Eastman') != -1): 
            eventLocation = "Eastman Hall"
        elif (img2.find('Lyon') != -1): 
            eventLocation = "Lyon Hall"
        elif (img2.find('Ithaca Falls') != -1): 
            eventLocation = "Ithaca Falls Room"
        elif (img2.find('Landon') != -1): 
            eventLocation = "Landon Hall"
        elif (img2.find('Tallcott') != -1): 
            eventLocation = "Tallcott Hall"
        elif (img2.find('Hood') != -1): 
            eventLocation = "Hood Hall"
        elif (img2.find('Six Mile Creek') != -1): 
            eventLocation = "Six Mile Creek Room"
        elif (img2.find('Cayuga Lake Room') != -1): 
            eventLocation = "Cayuga Lake Room"
        elif (img2.find('Textor') != -1): 
            eventLocation = "Textor Hall"
        elif (img2.find('Clark Lounge') != -1): 
            eventLocation = "Clark Lounge"
        elif (img2.find('Rowland') != -1): 
            eventLocation = "Rowland Hall"
        else:
            eventLocation = "Ithaca College"

        print(eventDate)
        print(eventStartTime)
        print(eventEndTime)
        print(eventTitle)
        print(eventLocation)
        self.eventDate = eventDate
        self.eventStartTime = eventStartTime
        self.eventEndTime = eventEndTime
        self.eventTitle = eventTitle
        self.eventLocation = eventLocation
    def getEventDate(self):
        return self.eventDate
    def getEventStartTime(self):
        return self.eventStartTime
    def getEventEndTime(self):
        return self.eventEndTime
    def getEventTitle(self):
        return self.eventTitle
    def getEventLocation(self):
        return self.eventLocation
        

                     
