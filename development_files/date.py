#!/usr/bin/env python
import re

class Date:
    def __init__(self,ss):
        self.string=ss
        self.time=None
        self.month=None
        s=ss.lower()
        suf='(\s\d\d\d\d)?\s?\d?\d?(st)?(rd)?(th)?(nd)?,?(\s\d\d\d\d)?'
        pre='(\d{1,2}\s)?'
        dates=r'(\d{1,2}/\d{1,2}((/\d\d\d\d)?(/\d\d)?))|'+pre+'january'+suf+'|february'+suf+'|march'+suf+'|april'+suf+'|may'+suf+'|june'+suf+'|july'+suf+'|august'+suf+'|september'+suf+'|october'+suf+'|november'+suf+'|december'+suf+'|jan'+suf+'|feb'+suf+'|mar'+suf+'|apr'+suf+'|jun'+suf+'|jul'+suf+'|aug'+suf+'|sept'+suf+'|oct'+suf+'|nov'+suf+'|dec'+suf
        times='((\d\d?:\d\d\s?(PM|pm|Pm|AM|am|Am)?)|(\d\d?:?(\d\d)?\s?(PM|Pm|pm|AM|Am|am)))'
        times=times+'(-'+times+')?'
        d=re.compile(dates)
        t=d.search(s)
        if(t!=None):
            self.month=t.group()
        dd=re.compile(times)
        ti=dd.search(s)
        if(ti)!=None:
            self.time=ti.group()

    def getdate(self):
        d=''
        m=''
        y=''
        if(self.month!=None):
            """
            if(self.month[0].isdigit()):
                if(self.month[1].isdigit()):
                    d=self.month[:2]
                else:
                    d=self.month[0]
            """
            if(True):
                mo=True
                if('jan' in self.month):
                    m='1'
                    mo=False
                if('feb' in self.month):
                    m='2'
                    mo=False
                if('mar' in self.month):
                    m='3'
                    mo=False
                if('apr' in self.month):
                    m='4'
                    mo=False
                if('may' in self.month):
                    m='5'
                    mo=False
                if('jun' in self.month):
                    m='6'
                    mo=False
                if('jul' in self.month):
                    m='7'
                    mo=False
                if('aug' in self.month):
                    m='8'
                    mo=False
                if('sep' in self.month):
                    m='9'
                    mo=False
                if('oct' in self.month):
                    m='10'
                    mo=False
                if('nov' in self.month):
                    m='11'
                    mo=False
                if('dec' in self.month):
                    m='12'
                    mo=False
                if(mo):
                    if(self.month[1].isdigit()):
                        m=self.month[0:2]
                    else:
                        m=self.month[0]
                else:
                    if(self.month[0].isdigit()):
                        if(self.month[1].isdigit()):
                            d=self.month[:2]
                        else:
                            d=self.month[0]




            if('/'in self.month):
                try:
                    if(self.month[self.month.find('/')+2].isdigit()):
                        d=self.month[self.month.find('/')+1:self.month.find('/')+3]
                    else:
                        d=self.month[self.month.find('/')+1]
                except:
                    d=self.month[self.month.find('/')+1]

            year=False
            try:
                if(self.month[-1].isdigit() and self.month[-2].isdigit() and self.month[-3].isdigit() and self.month[-4].isdigit()):
                    y=self.month[-4:]
                    year=True

            except:
                y=''
            if(self.month.count('/')==2):
                if(len(self.month[self.month.rfind('/')+1:])==2):
                    y='20'+self.month[self.month.rfind('/')+1:]
            if(not mo):
                if(self.month[0].isalpha()):
                    i=0
                    try:
                        while(self.month[i].isalpha()):
                            i+=1

                        while(not self.month[i].isdigit()):
                            i+=1

                        if(not self.month[i+1].isdigit()):
                            d=self.month[i]
                        elif(not self.month[i+2].isdigit()):
                            d=self.month[i:i+2]
                    except:
                        if(self.month[-1].isdigit()):
                            if(self.month[-2].isdigit()):
                                d=self.month[-2:]
                            else:
                                d=self.month[-1]
                        pass


            if(y==''):
                y='2019'
            if(d==''):
                d='26'
            if(m==''):
                m='10'
            if(len(d)==1):
                d='0'+d
            if(len(m)==1):
                m='0'+m

            return y+'-'+m+'-'+d
        return '2019-01-01'
    def gettime(self):
        if(self.time!=None):
            if(self.time[1].isdigit()):
                h=self.time[:2]
            else:
                h=self.time[0]
            if(':'in self.time):
                m=self.time[self.time.find(':'):
                self.time.find(':')+3]
            else:
                m=':00'
            ap=''
            if('PM' in self.time or 'pm' in self.time):
                ap=' PM'
            if('AM' in self.time or 'am' in self.time):
                ap=' AM'
            if(ap==' PM'):
                h=str(int(h)+12)
            if(len(m)==1):
                m='0'+m
            if(len(h)==1):
                h='0'+h
            if(h=='24'):
                h='00'
            h2=h

            h2=str(int(h)+2)
            if(h2=='25'):
                h2='01'
            if(h2=='24'):
                h2='00'

            return [h+m+':00',h2+m+':00']

        return ['00:00:00','02:00:00']
